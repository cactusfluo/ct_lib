
#ifndef BASICS_H
#define BASICS_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define ABS(x)					(((x) < 0) ? -(x): (x))
#define MAX(x, y)				(((x) > (y)) ? (x) : (y))
#define MIN(x, y)				(((x) > (y)) ? (y) : (x))
#define SIGN(x)					(((x) < 0) ? -1 : (((x) > 0) ? 1 : 0))
#define FLOOR(x)				((i64)(x))
#define CEIL(x)					(((x) - (i64)(x) == 0) ? \
								(i64)(x) : (i64)(x) + 1)
#define ROUND(x)				(((x) - (i64)(x) < 0.5) ?	\
								(i64)(x) : (i64)(x) + 1)

#define ABS2(x)					(g_basics_d = (x), ABS(g_basics_d))
#define MAX2(x)					(g_basics_d = (x), MAX(g_basics_d))
#define MIN2(x)					(g_basics_d = (x), MIN(g_basics_d))
#define SIGN2(x)				(g_basics_d = (x), SIGN(g_basics_d))
#define CEIL2(x)				(g_basics_d = (x), CEIL(g_basics_d))
#define ROUND2(x)				(g_basics_d = (x), ROUND(g_basics_d))

#define LERP(t, from, to)		((from) + (t) * ((to) - (from)))
#define NORM(t, from, to)		(((t) - (from)) / ((to) - (from)))
#define CONSTRAIN(x, min, max)	(MAX(MIN((max), (x)), (min)))
#define MAP(x, from1, to1, from2, to2) \
			((double)(from2) + (((double)(x) - (double)(from1)) \
			/ ((double)(to1) - (double)(from1))) \
			* ((double)(to2) - (double)(from2)))
#define MAPC(x, from1, to1, from2, to2) \
			(from2 <= to2) ? \
			CONSTRAIN(MAP(x, from1, to1, from2, to2), from2, to2) : \
			CONSTRAIN(MAP(x, from1, to1, from2, to2), to2, from2)

#define ODD(x)					((x) & 1)
#define EVEN(x)					(!ODD(x))

#define BIT_GET(x, y)			((uint8_t)(((x) >> (y)) & 1))
#define BIT_SET(x, y)			((x) | ((uint64_t)1 << (y)))
#define BIT_LET(x, y)			((x) & (UINT64_MAX ^ ((uint64_t)1 << (y))))
#define BIT_REV(x, y)			((x) ^ ((uint64_t)1 << (y)))

#define true					1
#define false					0

#define COLOR_NO				"\033[0m"
#define COLOR_RED				"\033[31;01m"
#define COLOR_GREEN				"\033[32;01m"
#define COLOR_YELLOW			"\033[33;01m"
#define COLOR_BLUE				"\033[34;01m"
#define COLOR_PINK				"\033[35;01m"
#define COLOR_CYAN				"\033[36;01m"
#define COLOR_WHITE				"\033[37;01m"

#define READ_BUFFER				2048
#define ARRM_BUFFER				32

////////////////////////////////////////////////////////////////////////////////
/// TYPES
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// TYPEDEF
//////////////////////////////////////////////////

typedef int8_t			bool;
typedef uint8_t			byte;
typedef int8_t			i8;
typedef uint8_t			u8;
typedef int16_t			i16;
typedef uint16_t		u16;
typedef int32_t			i32;
typedef uint32_t		u32;
typedef int64_t			i64;
typedef uint64_t		u64;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

extern double		g_basics_d;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

int					read_line(int fd, char **line);
ssize_t				read_file(int fd, char **file);

//////////////////////////////////////////////////
/// MEMORY
//////////////////////////////////////////////////

void				*mem_alloc(size_t size);
void				*mem_join(void const *p1, size_t len_1, void const *p2,
					size_t len_2);
void				mem_del(void **ap);

void				*mem_set(void *b, char c, size_t len);
void				mem_clean(void *s, size_t n);
void				*mem_cpy(void *dst, const void *src, size_t n);
void				*mem_ccpy(void *dst, const void *src, int32_t c, size_t n);
void				*mem_move(void *dst, const void *src, size_t len);
void				*mem_chr(const void *s, int32_t c, size_t n);
int					mem_cmp(const void *s1, const void *s2, size_t n);
void				**arr_new(size_t w, size_t h, size_t size);
void				*arr_mnew(size_t size, int dimensions, ...);

void				sort(void *array, int length, int elem_size,
					int (*f)(void*, void*));
void				sort_ptr(void **array, int length, int (*f)(void*, void*));

//////////////////////////////////////////////////
/// MESSAGES
//////////////////////////////////////////////////

void				mes_success(char *message);
void				mes_failure(char *message);
void				mes_warning(char *subtitle, char *message);
void				exit_fail(char *message);

#endif
