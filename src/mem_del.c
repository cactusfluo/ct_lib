
#include "basics.h"

void	mem_del(void **ap) {
	if (ap == NULL || *ap == NULL)
		return;
	free(*ap);
	*ap = NULL;
}
