/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "basics.h"
#include <stdarg.h>

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static size_t	get_init_dimensions(int dimensions, va_list args,
				size_t buffer[], size_t *nodes) {
	int			i;
	size_t		units;
	size_t		nodes_to_add;

	units = 1;
	*nodes = 0;
	nodes_to_add = 1;
	for (i = 0; i < dimensions; ++i) {
		buffer[i] = (size_t)va_arg(args, size_t);
		units *= buffer[i];
		if (i < dimensions - 1) {
			nodes_to_add = nodes_to_add * buffer[i];
			*nodes = *nodes + nodes_to_add;
		}
	}
	return units;
}

static void		**build_dimensions(int dimensions, size_t dim_list[],
				void **array, int index, size_t unit_size) {
	size_t		i;
	void		**data;

	/// FINAL NODES
	if (index == dimensions - 1)
		return (void**)((char*)array + (dim_list[index] * unit_size));
	/// REGULAR NODES
	data = (void**)array + dim_list[index];
	for (i = 0; i < dim_list[index]; ++i) {
		array[i] = data;
		data = build_dimensions(dimensions, dim_list, data, index + 1,
		unit_size);
	}
	return data;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			*arr_mnew(size_t unit_size, int dimensions, ...) {
	va_list		args;
	size_t		dim_list[ARRM_BUFFER];
	size_t		total_size;
	size_t		len_units;
	size_t		len_nodes;
	void		*array;

	if (dimensions == 0 || dimensions > ARRM_BUFFER)
		return NULL;
	va_start(args, dimensions);
	/// CREATE ARRAY
	len_units = get_init_dimensions(dimensions, args, dim_list, &len_nodes);
	total_size = len_nodes * sizeof(void*) + len_units * unit_size;
	array = (void*)malloc(total_size);
	mem_clean(array, total_size);
	/// FILL ARRAY (ADDRESS)
	build_dimensions(dimensions, dim_list, array, 0, unit_size);
	va_end(args);
	return array;
}
