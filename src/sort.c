/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "basics.h"

typedef int(*t_fcomp)(void*, void*);

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	swap(void **array, int a, int b, int size) {
	char	tmp;
	int		i;

	for (i = 0; i < size; ++i) {
		tmp = ((char*)array)[a * size + i];
		((char*)array)[a * size + i] = ((char*)array)[b * size + i];
		((char*)array)[b * size + i] = tmp;
	}
}

static int	partition(void **array, int start, int end, int size, t_fcomp f) {
	int			i, j;
	void		*pivot;

	pivot = (char*)array + end * size;
	i = start;
	for (j = start; j < end; ++j) {
		if (f((char*)array + j * size, pivot) < 0) {
			swap(array, i, j, size);
			i += 1;
		}
	}
	swap(array, i, end, size);
	return i;
}

static void		quicksort(void **array, int start, int end, int size, t_fcomp f) {
	int			pivot;

	if (start >= end)
		return;
	pivot = partition(array, start, end, size, f);
	quicksort(array, start, pivot - 1, size, f);
	quicksort(array, pivot + 1, end, size, f);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			sort(void *array, int length, int elem_size, t_fcomp f) {
	quicksort((void**)array, 0, length - 1, elem_size, f);
}
